using Controllers.Models;
using Data_Access.Interfaces;
using Data_Access.Repositories;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace Controllers.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Test", "Test", "Test", "Test", "Test", "Test", "Test"
        };
        private readonly IMessageRepository service;

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IMessageRepository repository)
        {
            _logger = logger;
            service = repository;
        }

        [HttpGet(Name = "GetWeatherForecast")]
        public IEnumerable<WeatherForecast> Get()
        {
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpPost(Name = "InsertMessage")]
        public Message InsertMessage(Message newMessage)
        {
            return service.InsertMessage(newMessage);
        }
    }
}