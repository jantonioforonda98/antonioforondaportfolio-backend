﻿using Data_Access.Repositories;
using Models;
using Services.Interfaces;

namespace Services.Services
{
    public class MessageService : IMessageService
    {
        public MessageRepository repository;
        public Message InsertMessage(Message newMessage)
        {
            return this.repository.InsertMessage(newMessage);
        }
    }
}
