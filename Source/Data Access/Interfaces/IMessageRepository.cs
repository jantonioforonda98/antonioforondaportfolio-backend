﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access.Interfaces
{
    public interface IMessageRepository
    {
        public Message InsertMessage(Message newMessage);
    }
}
