﻿namespace Models
{
    public class Message
    {
        public string sender { get; set; }

        public string message { get; set; }

        public int phone { get; set; }

        public string? email { get; set; }
    }
}