﻿using Models;
using Models.System;
using Dapper;
using System.Data.SqlClient;
using System.Data;
using Data_Access.Interfaces;

namespace Data_Access.Repositories
{
    public class MessageRepository : IMessageRepository
    {
        private readonly SQLConnection connection;

        public MessageRepository(SQLConnection sqlConnection)
        {
            this.connection = sqlConnection;
        }

        public Message InsertMessage(Message newMessage)
        {
            using IDbConnection db = new SqlConnection(this.connection.ConnectionString);
            var procedure = "Insert_Message";
            var parameters = new DynamicParameters();
            parameters.Add("@message", newMessage.message, DbType.String, ParameterDirection.Input);
            parameters.Add("@sender", newMessage.sender, DbType.String, ParameterDirection.Input);
            parameters.Add("@email", newMessage.email, DbType.String, ParameterDirection.Input);
            parameters.Add("@phone", newMessage.phone, DbType.Int32, ParameterDirection.Input);
            return db.Query<Message>(procedure, parameters, commandType: CommandType.StoredProcedure).First();
        }
    }
}